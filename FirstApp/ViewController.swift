//
//  ViewController.swift
//  FirstApp
//
//  Created by Adrián Zavala Coria on 14/11/17.
//  Copyright © 2017 Adrián Zavala Coria. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var pressed = false
    
    @IBOutlet weak var firstLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        firstLabel.text = "Hola"
        firstLabel.textColor = UIColor.blue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func gettingBackFromWolverineView(segue: UIStoryboardSegue!) {
        firstLabel.text = "Bienvenido de nuevo!"
    }
    
    @IBAction func gettingBackFromStackView(segue: UIStoryboardSegue!) {
        firstLabel.text = "Bienvenido de la StackView"
    }


    @IBAction func trigerEvent(_ sender: Any) {
        if(!pressed) {
            firstLabel.textColor = UIColor.red
            pressed = true
        } else {
            firstLabel.textColor = UIColor.brown
            pressed = false
        }
        
    }
}

